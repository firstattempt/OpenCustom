import React from 'react';
import { Modal, Button } from 'antd';
import { BaseComponent } from '../../base';
import './userLogin.less';
class UserLogin extends BaseComponent {
    state = {
        ModalText: 'Content of the modal',
        visible: false,
    }
    showModal = () => {
        this.setState({
            visible: true,
        });
    }
    handleOk = () => {
      this.setState({
        ModalText: 'The modal will be closed after two seconds',
        confirmLoading: true,
      });
      setTimeout(() => {
        this.setState({
          visible: false,
          confirmLoading: false,
        });
      }, 2000);
    }
    handleCancel = () => {
      console.log('Clicked cancel button');
      this.setState({
            visible: false,
      });
    }
    render() {
        const { visible, confirmLoading, ModalText } = this.state;
        return (
            <div>
            <Button type="primary" onClick={this.showModal}>Open</Button>
            <Modal title="Title"
                visible={visible}
            >
                <p>1234</p>
            </Modal>
            </div>
        );
    }
}