import React from 'react';
import { Layout } from 'antd';
import { BaseComponent } from '../../base';
import { AppBar,AppSider } from '../../components';
import './index.css';
const { Content } = Layout;
export default class Index extends BaseComponent {
    render(){
        return (
            <Layout>
                <AppBar/>
                <AppSider>
                    <Layout style={{ padding: '0 24px 24px' }}>
                        <Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 280 }}>
                            Content
                        </Content>
                    </Layout>
                </AppSider>
            </Layout>
        )
    }
}